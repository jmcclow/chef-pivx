#
# Cookbook:: eclectic_pivx
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

cookbook_file "/tmp/#{node['eclectic_pivx']['tarball']}" do
  source 'default/pivx-2.2.1-x86_64-linux-gnu.tar.gz'
  action :create
end

bash 'extract pivx' do
  code <<-EOM
    tar xvfz /tmp/#{node['eclectic_pivx']['tarball']} -C /opt/
  EOM
end

link '/opt/pivx' do
  to "/opt/#{node['eclectic_pivx']['package']}"
end

directory '/home/vagrant/.pivx'

template "/home/vagrant/.pivx/pivx.conf" do
  source 'default/pivx.conf.erb'
  action :create
end

directory '/root/.pivx'

template "/root/.pivx/pivx.conf" do
  source 'default/pivx.conf.erb'
  action :create
end

bash 'start pivxd' do
  user 'root'
  code <<-EOM
    /opt/pivx/bin/pivxd
  EOM
end
